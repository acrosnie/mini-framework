<?php
 class mainController
 {
	public function __construct()
	{
	}

	public function render($renderName, $param = array())
	{
		if (file_exists('View/' . $renderName))
		{
			foreach ($param as $key => $value)
			{
				if (is_numeric($key))
				{
					print("Error, the key can't be numeric (function render)");
					die();
				}
				$template[$key] = $value;
			}
			return require_once('View/' . $renderName);
		}
		else
		{
			print("Error, $renderName doesn't exist in View (function render)");
		}
		return false;
	}
 }