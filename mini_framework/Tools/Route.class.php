<?php

class Route
{
	protected $path;
	protected $parametre;
	protected $controllerClassName;
	protected $controllerMethodName;

	public function __construct($path, $controllerClassName, $controllerMethodName, $parametre = array())
	{
		$this->path = $path;
		$this->controllerClassName = $controllerClassName;
		$this->controllerMethodName = $controllerMethodName;
		$this->parametre = $parametre;
	}

	public function getPath()
	{
		return $this->path;
	}

	public function getControllerClassName()
	{
		return $this->controllerClassName;
	}

	public function getControllerMethodName()
	{
		return $this->controllerMethodName;
	}

	public function getParameter()
	{
		// $p = "";
		// foreach ($this->parametre as $key => $value) {
		// 	$p .= $value . ",";
		// }
		// $p = substr($p, 0, -1);
		return $this->parametre;
	}
}