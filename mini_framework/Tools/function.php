<?php

function calc_route()
{
	$myUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$myUrl = explode("app_dev.php", $myUrl);
	if (isset($myUrl[1]) && !empty($myUrl[1]) && $myUrl[1] != '/')
	{
		$route = substr($myUrl[1], 1);
		$myExplode = explode("/", $route);
		 return $myExplode[0];
	}
	return '/';
}

function calc_param()
{
	$param = array();
	$myUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$myUrl = explode("app_dev.php", $myUrl);
	if (isset($myUrl[1]) && !empty($myUrl[1]))
	{	
		$myExplode = explode("/", $myUrl[1]);
		foreach ($myExplode as $key => $value)
		{
			if (!empty($value) && calc_route() != $value)
				$param[] = $value;
		}
	}
	return $param;
}
