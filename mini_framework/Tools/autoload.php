<?php
function __autoload($class_name)
{
	if (file_exists('Tools/'.$class_name . '.class.php'))
		require_once 'Tools/'.$class_name . '.class.php';
	else if(file_exists('Controller/'.$class_name . '.class.php'))
		require_once 'Controller/'.$class_name . '.class.php';
	else
		require_once 'Modele/'.$class_name . '.class.php';
}