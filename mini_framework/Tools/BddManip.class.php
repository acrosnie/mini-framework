<?php
class BddManip
{
	private $host;
 	private $dbname;
 	private $user;
 	private $pass;
	protected $bdd;
	protected $table;

	public function __construct()
	{
		global $bddCongfig;
		$this->table = NULL;
		foreach ($bddCongfig as $key => $value)
			$this->$key = $value;
		$this->connect_database();
		if ($this->whoAmI() && $this->whoAmI() != "BddManip")
		{
			$this->table = strtolower($this->whoAmI());
			//$this->majChild(); ? !
		}
		
	}

	public function majChild($result)
	{
		foreach ($this->getChamps() as $key => $value)
		{
			$val = "set".ucfirst($value);
			$this->$val($result[0][$value]);
		}
	}

	public static function whoAmI()
	{
		return get_called_class();
	}

	public function connect_database()
	{
		try
		{
			$this->bdd = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);
			$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch (Exception $e)
		{
			die('Erreur : ' . $e->getMessage());
		}
	}

	public function findById($id)
	{
		if ($this->has_table() != NULL)
		{
			$sql = "SELECT * FROM ".$this->table." WHERE id = :id";
			$req = $this->bdd->prepare($sql);
			$req->execute(array("id" => $id));
			$result = $req->fetchAll(PDO::FETCH_ASSOC);
			$this->majChild($result);
			return $result;
		}
		return NULL;
	}

	public function findBy($champs, $value)
	{
		if ($this->has_table() != NULL)
		{
			$sql = "SELECT * FROM ".$this->table." WHERE ".$champs." = :champs";
			$req = $this->bdd->prepare($sql);
			$req->execute(array("champs" => $value));
			$result = $req->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
		return NULL;
	}

	public function Update()
	{
		$champs = "";
		$array = array();
		foreach ($this->getChamps() as $key => $value)
		{
			$val = "get".ucfirst($value);
			$array[$value] = $this->$val();
			$champs .= $value."= :" .$value.",";
		}
		$array["idWhere"] = $this->getId();
		$champs = substr($champs, 0, -1);
		$sql = "UPDATE ".$this->table." SET ".$champs." WHERE id = :idWhere";
		$req = $this->bdd->prepare($sql);
		$req->execute($array);
	}

	public function has_table()
	{
		if ($this->table == NULL && $this->table != "BddManip")
		{
			print("Can't connect to a NUll table !");
			return false;
		}
		return true;
	}
}