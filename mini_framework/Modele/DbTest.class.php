<?php
class DbTest extends BddManip
{
	protected $id; // int autoincrement
	protected $name; //varchar 255

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setId($id)
	{
		// don't safe, can crash
		$this->id = $id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getChamps() // NEED TO UPDATE
	{
		return array("id", "name");
	}
}